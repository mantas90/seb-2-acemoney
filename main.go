package main

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"os"
	"regexp"
	"strings"
)

var (
	myRegexp = regexp.MustCompile(`\s{2,}`)
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {

	if len(os.Args) != 2 {
		fmt.Println("add path as command line argument")
		return
	}

	// Open the file

	pathToSebFile := os.Args[1]

	fmt.Println(pathToSebFile)

	if !strings.HasSuffix(pathToSebFile, ".csv") {
		fmt.Println("should end with '.csv'")
		return
	}

	pathToAceFile := strings.TrimSuffix(pathToSebFile, ".csv") + "-out.csv"

	csvfile, err := os.Open(pathToSebFile)
	if err != nil {
		log.Fatalln("Couldn't open the csv file", err)
	}

	//skip row
	r2 := bufio.NewReader(csvfile)
	row1, err := r2.ReadSlice('\n')
	if err != nil {
		panic(err)
	}

	_, err = csvfile.Seek(int64(len(row1)), io.SeekStart)
	if err != nil {
		panic(err)
	}

	// Parse the file
	r := csv.NewReader(csvfile)
	r.Comma = ';'
	r.LazyQuotes = true
	//r.FieldsPerRecord = 19

	f, err := os.Create(pathToAceFile)
	check(err)

	// Iterate through the records

	defer f.Close()

	for {
		// Read each record from csv
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		check(err)

		seb := CsvRecordToSeb(record)
		ace := seb.ToAce()
		aceString := ace.ToCsvString()

		_, err = f.WriteString(aceString + "\n")
		check(err)
	}

	fmt.Println("DONE")
}

func Clean(s string) string {

	s = strings.Replace(s, "\\", " ", -1)
	s = myRegexp.ReplaceAllString(s, " ")

	s = strings.ReplaceAll(s, "ą", "a")
	s = strings.ReplaceAll(s, "č", "c")
	s = strings.ReplaceAll(s, "ę", "e")
	s = strings.ReplaceAll(s, "ė", "e")
	s = strings.ReplaceAll(s, "į", "i")
	s = strings.ReplaceAll(s, "š", "s")
	s = strings.ReplaceAll(s, "ų", "u")
	s = strings.ReplaceAll(s, "ū", "u")
	s = strings.ReplaceAll(s, "ž", "z")

	s = strings.ReplaceAll(s, "Ą", "A")
	s = strings.ReplaceAll(s, "Č", "C")
	s = strings.ReplaceAll(s, "Ę", "E")
	s = strings.ReplaceAll(s, "Ė", "E")
	s = strings.ReplaceAll(s, "Į", "I")
	s = strings.ReplaceAll(s, "Š", "S")
	s = strings.ReplaceAll(s, "Ų", "U")
	s = strings.ReplaceAll(s, "Ū", "U")
	s = strings.ReplaceAll(s, "Ž", "Z")
	return s
}

func CsvRecordToSeb(record []string) *SebInput {
	s := SebInput{
		MOKĖTOJO_ARBA_GAVĖJO_PAVADINIMAS: record[4],
		MOKĖJIMO_PASKIRTIS:               record[9],
		DOKUMENTO_DATA:                   record[11],
		DEBETAS_KREDITAS:                 record[14],
		SUMA_SĄSKAITOS_VALIUTA:           record[15],
	}
	return &s
}

func (seb *SebInput) ToAce() AceOutput {
	ace := AceOutput{}
	//ace.A_TransactionNr = seb.
	ace.B_Date = seb.DOKUMENTO_DATA
	//ace.C_Payee,
	//ace.D_Category,
	//ace.E_Status,
	if seb.DEBETAS_KREDITAS == "D" {
		ace.F_Withdrawal = seb.SUMA_SĄSKAITOS_VALIUTA
	}

	if seb.DEBETAS_KREDITAS == "C" {
		ace.G_Deposit = seb.SUMA_SĄSKAITOS_VALIUTA
	}

	//ace.G_Deposit,
	//ace.H_Total,
	ace.I_Comment = fmt.Sprintf("%s (%s)", Clean(seb.MOKĖTOJO_ARBA_GAVĖJO_PAVADINIMAS), Clean(seb.MOKĖJIMO_PASKIRTIS))
	return ace
}

func (ace *AceOutput) ToCsvString() string {
	s := fmt.Sprintf("%s;%s;%s;%s;%s;%s;%s;%s;%s", ace.A_TransactionNr, ace.B_Date, ace.C_Payee, ace.D_Category, ace.E_Status, ace.F_Withdrawal, ace.G_Deposit, ace.H_Total, ace.I_Comment)
	return s
}

type SebInput struct {
	DOK_NR,
	DATA,
	VALIUTA,
	SUMA,
	MOKĖTOJO_ARBA_GAVĖJO_PAVADINIMAS,
	MOKĖTOJO_ARBA_GAVĖJO_IDENTIFIKACINIS_KODAS,
	SĄSKAITA,
	KREDITO_ĮSTAIGOS_PAVADINIMAS,
	KREDITO_ĮSTAIGOS_SWIFT_KODAS,
	MOKĖJIMO_PASKIRTIS,
	TRANSAKCIJOS_KODAS,
	DOKUMENTO_DATA,
	TRANSAKCIJOS_TIPAS,
	NUORODA,
	DEBETAS_KREDITAS,
	SUMA_SĄSKAITOS_VALIUTA,
	SĄSKAITOS_NR,
	SĄSKAITOS_VALIUTA string
}

type AceOutput struct {
	/*
	* A (1) - Transaction/check number
	* B (2) - Date
	* C (3) - Payee
	* D (4) - Category
	* E (5) - Status
	* F (6) - Withdrawal
	* G (7) - Deposit
	* H (8) - Total
	* I (9) - Comment
	 */

	A_TransactionNr,
	B_Date,
	C_Payee,
	D_Category,
	E_Status,
	F_Withdrawal,
	G_Deposit,
	H_Total,
	I_Comment string
}
